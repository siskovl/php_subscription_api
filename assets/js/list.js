function getHandler()
{
    jQuery.ajax({
        url: "/api/subscriber/myList",
        type: "Get",
        dataType: 'json',
        error: function (a, b, c) {
            console.log('Error while submitting form: ' + b);
        },
        success: function (response) {
            displayArray(response.data);
        }
    });
}

//After the Html is loaded

function displayArray(data) {

    var text = "";
    for (var i in data) {
        text += "<tr>";
//        text += "<td>" + data[i].id+ "</td>";
        text += "<td>" + data[i].name + "</td>";
        text += "<td>" + data[i].email + "</td>";
        text += "</tr>";
    }
    document.getElementById("trlist").innerHTML = text;
}

$(document).ready(function () {
    getHandler();
});
