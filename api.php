<?php
// set response header to be JSON type
// with character set UTF-8
header('Content-Type: application/json; charset=UTF-8');

// param uri passed by .htaccess
$uri = isset($_GET['uri']) ? $_GET['uri'] : ''; // get the uri so we can work with ContextProcessor

// if uri exists
if ($uri)
{
    include __DIR__ .'/Lib/autoload.php';
    include __DIR__ . '/autoload.php';
    $cp = new MyApi\ContextProcessor(); // instantiate ContextProcessor()
    $cp->process($uri); // we create this function
    echo json_encode($cp->getOutputAsArray()); //display 
}
else
{
    echo json_encode(array('error'=>'Illegal request'));
}