<?php

namespace MyApi\Services;

class Subscriber extends \MyApi\ContextProcessorServiceAbstract {

    private $uriParts = array();

    public function setUriParts(array $uriParts) {
        $this->uriParts = $uriParts; // api/contact/processdata --> here we have [processdata]
    }

    public function execute() {
        // check if 
        if (sizeof($this->uriParts) && $this->uriParts[0]) {
            if (method_exists($this, $this->uriParts[0])) {
                $this->{$this->uriParts[0]}(); // if it exists then execute the path
            } else {
                $this->output = array('error' => 'Method ' . $this->uriParts[0]); // if it doesnt exist then trow an error
            }
        } else {
            $this->output = array('error' => 'Illegal request.');
        }
    }

    private function newsletter() {



        $dbSettings = new \Database\DbSettings('mysql', 'localhost', 'yourUsernameHere', 'yourPasswordHere');
        $dbo = new \Database\Dbo($dbSettings);

        $statement = 'INSERT INTO dbName.tbName(name,email) VALUES('
                . $dbo->quote($_POST['name'])
                . ',' . $dbo->quote($_POST['email']) . ')'; // how to insert data to database              
        $dbo->query($statement);

        $this->output = array(
            'success' => true,
            'message' =>  json_encode($_POST ['name']).', you are now subscribed!' 
        );
    }
    
    private function myList(){
        $dbSettings = new \Database\DbSettings('mysql', 'localhost', 'yourUsernameHere', 'yourPasswordHere');
        $dbo = new \Database\Dbo($dbSettings);
        
        $stm = "SELECT name, email FROM dbName.tbName";

        
        $row = $dbo->loadAssocList($stm);
        $this->output = array(
            'data' => $row,
            'success' => true,
            'message' => 'Successfully processed!' . json_encode($_POST)
        );
    }
    
//. json_encode($_POST)
}
