<?php

namespace MyApi\Services;

class SubsList extends \MyApi\ContextProcessorServiceAbstract {

    private $uriParts = array();

    public function setUriParts(array $uriParts) {
        $this->uriParts = $uriParts; // api/contac/processdata --> here we have [processdata]
    }

    public function execute() {
        // check if 
        if (sizeof($this->uriParts) && $this->uriParts[0]) {
            if (method_exists($this, $this->uriParts[0])) {
                $this->{$this->uriParts[0]}(); // if it exists then execute the path
            } else {
                $this->output = array('error' => 'Method ' . $this->uriParts[0]); // if it doesnt exist then trow an error
            }
        } else {
            $this->output = array('error' => 'Illegal request.');
        }
    }

    // if we change this to vote: ---> Email or rewrite to database
    private function mylist() {

        // Assume $db is a PDO object
        $query = $db->query("SELECT * FROM dbName.tbName"); // Run your query

        echo '<select name="dropdown">'; // Open your drop down box
        // Loop through the query results, outputing the options one by one
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            echo '<option value="' . $row['something'] . '">' . $row['something'] . '</option>';
        }

        echo '</select>'; // Close your drop down box
    }

}
