<?php

namespace MyApi;

class ContextProcessor
{
    private $output = array();
    
    public function process($uri)
    {
        $parts = explode('/', $uri);
        
        if (sizeof($parts) > 1) // parts should be more than 1
        {
            $parts[0] = ucwords($parts[0]);
            $service = 'MyApi\Services\\'.$parts[0];
            $serviceObj = new $service; // anonymous function from string to object 
            
            if ($serviceObj instanceof ContextProcessorServiceAbstract)
            {
                unset($parts[0]); // delete from the array --find my guy so i delete it from not make mistake after
                $uriParts = explode('/', implode('/', $parts)); // implode :take the string makes an array like join();1/2/3/4/5/n
                                                                // explode [0,1,2,3,4...,n]
                $serviceObj->setUriParts($uriParts);            
                $serviceObj->execute();
                $this->output = $serviceObj->getOutputAsArray();
            }
        }
    }
    
    public function getOutputAsArray()
    {
        return $this->output;
    }
}